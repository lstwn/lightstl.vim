# lightstl.vim lightweight statusline plugin for vim

> the statusline plugin I wish it had existed..

A lightweight statusline plugin for vim that embraces simplicity and
vim's way of handling statuslines.
A lot of what much bigger statusline plugins achive can be done with less:
This plugin consists of only ~150 lines of code that you could have written
yourself.

It assumes no defaults and it is *your* responsibility to compose *your*
statusline. In fact, it will not even load if not configured by *you*
in your `.vimrc`.

## Installation

Use your favourite vim plugin manager. For instance, with `vim-plug`:

```
Plug 'https://gitlab.com/lstwn/lightstl.vim'
```

Then set in your `.vimrc`:

```
TODO
```

## Customization and Usage

TODO

