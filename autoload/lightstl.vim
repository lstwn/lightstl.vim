function lightstl#CreateSection(
            \   focused_text = '', focused_highlight = '',
            \   unfocused_text = a:focused_text,
            \   unfocused_highlight = a:focused_highlight
            \ ) abort
    return
                \ {
                \   'focused': {
                \     'text': a:focused_text, 'highlight': a:focused_highlight,
                \   },
                \   'unfocused': {
                \     'text': a:unfocused_text, 'highlight': a:unfocused_highlight,
                \   },
                \ }
endfunction

function lightstl#AddMainSection(section) abort
    call funcref('lightstl#AddSection', ['s:lightstl_main_config'])(a:section)
endfunction

function lightstl#AddHelpSection(section) abort
    call funcref('lightstl#AddSection', ['s:lightstl_help_config'])(a:section)
endfunction

function lightstl#AddNetrwSection(section) abort
    call funcref('lightstl#AddSection', ['s:lightstl_netrw_config'])(a:section)
endfunction

function lightstl#AddQuickfixSection(section) abort
    call funcref('lightstl#AddSection', ['s:lightstl_quickfix_config'])(a:section)
endfunction

function lightstl#AddTerminalSection(section) abort
    call funcref('lightstl#AddSection', ['s:lightstl_terminal_config'])(a:section)
endfunction

function lightstl#AddSection(to, section) abort
    if !exists(a:to)
        execute 'let ' . a:to . ' = []'
    endif
    execute 'call add(' . a:to . ', a:section)'
endfunction

augroup lightstl
    autocmd!
augroup END

function lightstl#Apply() abort
    if exists('s:lightstl_main_config')
        let g:LightstlMainConfigured = lightstl#Configure(s:lightstl_main_config)
        set statusline=%!g:LightstlMainConfigured()
        unlet s:lightstl_main_config
    endif
    if exists('s:lightstl_help_config')
        let g:LightstlHelpConfigured = lightstl#Configure(s:lightstl_help_config)
        augroup lightstl
            autocmd FileType help setlocal statusline=%!g:LightstlHelpConfigured()
        augroup END
        unlet s:lightstl_help_config
    endif
    if exists('s:lightstl_netrw_config')
        let g:LightstlNetrwConfigured = lightstl#Configure(s:lightstl_netrw_config)
        augroup lightstl
            autocmd FileType netrw setlocal statusline=%!g:LightstlNetrwConfigured()
        augroup END
        unlet s:lightstl_netrw_config
    endif
    if exists('s:lightstl_quickfix_config')
        let g:LightstlQuickfixConfigured = lightstl#Configure(s:lightstl_quickfix_config)
        augroup lightstl
            autocmd FileType qf setlocal statusline=%!g:LightstlQuickfixConfigured()
        augroup END
        unlet s:lightstl_quickfix_config
    endif
    if exists('s:lightstl_terminal_config')
        let g:LightstlTerminalConfigured = lightstl#Configure(s:lightstl_terminal_config)
        augroup lightstl
            autocmd TerminalWinOpen * setlocal statusline=%!g:LightstlTerminalConfigured()
        augroup END
        unlet s:lightstl_terminal_config
    endif
endfunction

function lightstl#Configure(config) abort
    let status = { 'focused': '', 'unfocused': '' }
    for group in a:config
        let focused_highlight = get(group.focused, 'highlight', '')
        let focused_text = get(group.focused, 'text', '')
        let status.focused .= focused_highlight . focused_text
        let unfocused_highlight = get(group.unfocused, 'highlight', focused_highlight)
        let unfocused_text = get(group.unfocused, 'text', focused_text)
        let status.unfocused .= unfocused_highlight . unfocused_text
    endfor
    return funcref('lightstl#Get', [status])
endfunction

function lightstl#Get(config) abort
    if g:statusline_winid == win_getid()
        return a:config.focused
    else
        return a:config.unfocused
    endif
endfunction

