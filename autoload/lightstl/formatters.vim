function lightstl#formatters#List(prefix = '[', separator = ',', suffix = ']', list = []) abort
    let l:list = filter(a:list, 'v:val !=# ""')
    if len(l:list) == 0
        return ''
    endif
    let l:formatted = l:list[0]
    for l:item in l:list[1:-1]
        let l:formatted .= a:separator . l:item
    endfor
    return lightstl#formatters#Item(a:prefix, a:suffix, l:formatted)
endfunction

function lightstl#formatters#Item(prefix = '', suffix = '', item  = '') abort
    if a:item ==# ''
        return ''
    endif
    return a:prefix . a:item . a:suffix
endfunction

