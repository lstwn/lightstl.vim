if !exists('s:lightstl_components_gitbranch')
    let s:lightstl_components_gitbranch = ''
endif

function lightstl#components#gitbranch#Get() abort
    return s:lightstl_components_gitbranch
endfunction

function lightstl#components#gitbranch#Set() abort
    let l:query = systemlist("git rev-parse --abbrev-ref HEAD 2>/dev/null")
    if len(l:query) != 1
        return
    endif
    let s:lightstl_components_gitbranch = l:query[0]
endfunction
command -nargs=0 LightstlSetGitBranch call lightstl#components#gitbranch#Set()
LightstlSetGitBranch

augroup lightstl
    autocmd DirChanged * call lightstl#components#gitbranch#Set()
augroup END

