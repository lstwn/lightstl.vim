" checks for trailing whitespace and returns first affected line starting
" from current cursor position; returns 0 if none found
function lightstl#components#fileinfo#NextTrailingWhitespace() abort
    return search('\s\+$', 'nw', 0, 300)
endfunction

" checks for git conflict markers and returns first affected line starting
" from current cursor position; returns 0 if none found
function lightstl#components#fileinfo#NextConflict() abort
    let l:annotation = '\%([0-9A-Za-z_.:]\+\)\?'
    if &ft is# 'rst'
        " rst filetypes use '=======' as header
        let l:pattern = '^\%(\%(<\{7} '.l:annotation.'\)\|\%(>\{7\} '.l:annotation.'\)\)$'
    else
        let l:pattern = '^\%(\%(<\{7} '.l:annotation.'\)\|\%(=\{7\}\)\|\%(>\{7\} '.l:annotation.'\)\)$'
    endif
    return search(l:pattern, 'nw', 0, 300)
endfunction

" checks for mixed indent and returns first affected line starting
" from current cursor position; returns 0 if none found
function lightstl#components#fileinfo#NextMixedIndent() abort
    return search('\v(^\t+ +)|(^ +\t+)', 'nw', 0, 300)
endfunction

