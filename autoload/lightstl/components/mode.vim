function lightstl#components#mode#Get() abort
    let l:mode = mode(1)
    if l:mode[0] ==# "i"
        return 'INSERT'
    elseif l:mode[0] ==# "R"
        return 'REPLACE'
    elseif l:mode[0] =~# '\v(v|V|\W|s|S)'
        return 'VISUAL'
    elseif l:mode ==# "t"
        return 'TERMINAL'
    elseif l:mode[0] ==# "c"
        return 'COMMANDLINE'
    endif
    return 'NORMAL'
endfunction

